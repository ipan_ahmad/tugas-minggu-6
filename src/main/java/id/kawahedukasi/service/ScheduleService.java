package id.kawahedukasi.service;

import id.kawahedukasi.model.Item;
import io.quarkus.scheduler.Scheduled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Schedule;
import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

@ApplicationScoped
public class ScheduleService {
    Logger logger = LoggerFactory.getLogger(ScheduleService.class);

    @Scheduled(every = "1h")
    @Transactional
    public void deletedItemCountZero(){
        Long deletedItem = Item.delete("count=?1",0);
        logger.info("Delete Item -> {}", deletedItem);

    }
}
