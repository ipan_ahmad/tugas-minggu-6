package id.kawahedukasi.service;

import id.kawahedukasi.model.Item;
import io.vertx.core.json.JsonObject;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;

@ApplicationScoped
public class ItemService {

    @Transactional
    public Response create(JsonObject request){
        Item item = new Item();
        item.name = request.getString("name");
        item.count = request.getInteger("count");
        item.type = request.getString("type");
        item.price = request.getLong("price");
        item.description = request.getString("description");

        item.persist();

        return Response.ok().entity(new HashMap<>()).build();
    }

    public Response getAll(){
        List<Item> itemList = Item.listAll();
        return Response.ok().entity(itemList).build();
    }

    @Path("/{id}")
    public Response list(@PathParam("id") Integer id){

        return Response.ok().entity(Item.findById(id)).build();
    }

    @Path("/{id}")
    @Transactional
    public Response update(@PathParam("id") Integer id, JsonObject request){
        Item item = Item.findById(id);
        item.name = request.getString("name");
        item.count = request.getInteger("count");
        item.type = request.getString("type");
        item.price = request.getLong("price");
        item.description = request.getString("description");

        item.persist();

        return Response.ok().entity(new HashMap<>()).build();
    }

    @Path("/{id}")
    @Transactional
    public Response delete(@PathParam("id") Integer id){
        Item.deleteById(id);
        return Response.ok().entity(new HashMap<>()).build();
    }


}
