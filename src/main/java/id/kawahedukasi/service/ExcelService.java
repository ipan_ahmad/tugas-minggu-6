package id.kawahedukasi.service;

import id.kawahedukasi.model.Item;
import id.kawahedukasi.model.dto.UploadItemRequest;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@ApplicationScoped
public class ExcelService {
    public Response upload(UploadItemRequest request) throws IOException {
        List<Item> itemList = new ArrayList<>();

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(request.file);
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(byteArrayInputStream);
        XSSFSheet sheet = xssfWorkbook.getSheetAt(0);

        sheet.removeRow(sheet.getRow(0));

        Iterator<Row> rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()){
            Item item = new Item();
            org.apache.poi.ss.usermodel.Row row = rowIterator.next();


            item.name = row.getCell(2).getStringCellValue();
            item.count = Double.valueOf(row.getCell(0).getNumericCellValue()).intValue();
            item.description = row.getCell(1).getStringCellValue();
            item.price = Double.valueOf(row.getCell(3).getNumericCellValue()).longValue();
            item.type = row.getCell(4).getStringCellValue();
            itemList.add(item);
        }

        persistListItem(itemList);
        return Response.ok().build();
    }

    @Transactional
    public void persistListItem(List<Item> itemList){
        Item.persist(itemList);
    }
}
